<?php

class Curl
{
	private $ch;

	private $header_display = false;

	private $proxy = "";

	private $header = array();

	private $enableCookies = true;

	private $cookieFile = "";

	private $userAgent = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:42.0) Gecko/20100101 Firefox/42.0";

	/**
	 * @var $response string
	 */
	private $response = '';

	private $is_error = false;

	private $error_msg = "";
	
	private $enforceSSLVerify = false;

	private $throwExceptionOnError = false;

	public $info = array();

	public $code = 0;


	/**
	 * Public constructor
	 * @param $url - string - URL for curl_init
	 * @param $proxy - string - PROXY server
	 */
	public function __construct($url = "", $proxy = ""){

		$this->ch = curl_init();

		if (!empty($url))
			$this->setUrl($url);
		
		if (!empty($proxy)){
			$this->setProxy($proxy);
		}
	}


	/*
	 * Enable Cookie tracing
	 * @param $enable boolean
	 * @param $file  string - File name to store cookie data
	 */
	public function enableCookies($enable, $file = ""){
		$this->enableCookies = $enable;
		if(!empty($file)){
			$this->cookieFile = $file;
		}
	}	


	/**
	 * 
	 * Max number of seconds allowed to connect the resource.
	 * @param boolean $timeout | default = 0 - indefinite
	 */
	public function connectionTimeout($timeout = 0){
		curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	}

	/**
	 *
	 * If object throws Exception when curl detects an error
	 */
	public function throwExceptionOnError($action){
		$this->throwExceptionOnError = $action? true : false;
	}


	/**
	 * Get request
	 * A Url must be set before to init curl 
	 */
	public function get($return = true){
		// ----------------------------------------------------------------------------------------
		// If url is empty throw a Curl Exception
		// ----------------------------------------------------------------------------------------
		if(empty(curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL)))
			throw new CurlException("No effective URL set for curl connection");
		$this->setHeader("Connection","keep-alive");
		return $this->execute($return);		
	}

	/**
	 * @method getJSONResponse
	 * Returns an PHP object mirroring the JSON representation
	 */
	public function getJSONResponse(){
		$this->setHeader("Connection","keep-alive");
		$this->execute(true);	
		return json_decode($this->response);
	}



	/**
	 * Post request
	 * Url encodes all values
	 * Sets header to application/x-www-form-urlencoded
	 * @param $fields array
	 * @param $return boolean
	 */
	public function post($fields, $return = false){

		// Filteres urlnecoded fields
		$params = array();

		// ----------------------------------------------------------------------------------------
		// If url is empty throw a Curl Exception
		// ----------------------------------------------------------------------------------------
		if(empty(curl_getinfo($this->ch, CURLINFO_EFFECTIVE_URL)))
			throw new CurlException("No effective URL set for curl connection");

		// Check for correct param type
		if (is_array($fields)){

			foreach ($fields as $fk=>$fv){
				$params[] = $fk . "=" . urlencode($fv);
			}
		}
		else{
			throw InvalidArgumentException ("Curl::post expects an array for parameter");
		}

		$this->setHeader("Content-Type", "application/x-www-form-urlencoded");

		curl_setopt($this->ch,CURLOPT_POSTFIELDS, implode("&",$params));
		curl_setopt($this->ch, CURLOPT_POST, true);

		//$this->prettyPrint($params);

		return $this->execute ($return);
	}

	/**
	 * Set url
	 */
	public function setUrl($url){
		curl_setopt($this->ch, CURLOPT_URL, $url);
	}


	public function setBasicAuth ($user, $password){

		if (is_null($this->ch)){
			throw new Exception("Curl has not been initialized");
		}

		curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($this->ch,CURLOPT_USERPWD, "{$user}:{$password}");
	}

	/**
	 * Max execution time for the curl request
	 */
	public function setMaxExecutionTime($max_time){
		curl_setopt($this->ch, CURLOPT_TIMEOUT, $max_time);
	}

	/** 
	 * Enforce curl to verify SSL peer
	 */
	public function enforceSSLVerify($action){
		$this->enforceSSLVerify = $action? true : false;
	}


	public function setHeader($name,$value){
		$this->header[] = $name . ": " . $value;
	}

	public function setUserAgent($user_agent){
		$this->userAgent = trim($user_agent);
	}

	public function trackHeader($track = false){
		curl_setopt($this->ch,CURLINFO_HEADER_OUT,$track);
	}

	/**
	 * ShowHeader
	 * @display boolean true|false
	 */
	public function showHeader($display){
		$this->header_display = $display;
	}

	public function showRequestHeader(){
		$this->prettyPrint($this->info['request_header']);
	}

	public function isError(){
		return $this->is_error;
	}

	public function errorMsg(){
		return $this->error_msg;
	}
	

	public function prettyPrint($smth){
		print "<pre>";
		print_r($smth);
		print "</pre>";

	}

	// Public destructor
	public function __destruct(){
		curl_close($this->ch);
	}


	/**
	 * Private methods
	 */


	private function execute($return = true){

		// Set custom headers if set
		if(!empty($this->header))
			curl_setopt($this->ch,CURLOPT_HTTPHEADER, $this->header);

		if ($this->enableCookies){
			curl_setopt($this->ch,CURLOPT_COOKIEJAR,$this->cookieFile);
			curl_setopt($this->ch,CURLOPT_COOKIEFILE,$this->cookieFile);
		}

		// Set SSL verifications	
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, $this->enforceSSLVerify);		

		if (!empty($this->userAgent))
			curl_setopt($this->ch, CURLOPT_USERAGENT, $this->userAgent);	

		// Return response as string or output to screen
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, $return);
		// Follow location
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
		// Retrieve header output
		curl_setopt($this->ch, CURLOPT_HEADER, $this->header_display);		

		$this->response = curl_exec($this->ch);

		$this->info = curl_getinfo($this->ch);

		if(curl_errno($this->ch) != 0){
			$this->is_error = true;
			$this->error_msg = curl_error($this->ch);

			if($this->throwExceptionOnError){
				throw new CurlException($this->error_msg);
			}
		}

		$this->code = $this->info['http_code'];

		return $this->response;
	}
}


class CurlException extends Exception{}